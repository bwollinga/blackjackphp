<?php
    require 'components/deck.php';
    require 'components/players.php';
    require 'components/engine.php';
    require 'components/api.php';
    
    // onderstaande regel kan weg omdat deze al in engine.php wordt uitgevoerd
    if(!isset($_SESSION)) { session_start(); }

    // restore engine from session
    if(isset($_SESSION["engine"])) {
        $engine = $_SESSION["engine"];
    } else {
        $engine = new Engine();
    }

    $api = new Api($engine);  
    $api->route($_SERVER['REQUEST_URI']);

    // store engine in session
    $_SESSION["engine"] = $engine;
?>