// je kunt hier consts van maken
var title       = document.getElementById("title");
var dealername  = document.getElementById("dealername");
var dealerhand  = document.getElementById("dealerhand");
var playername  = document.getElementById("playername");
var playerhand  = document.getElementById("playerhand");
var playerscore = document.getElementById("playerscore");
var hitbutton   = document.getElementById("hit");
var standbutton = document.getElementById("stand");
var message     = document.getElementById("message");
var resetbutton = document.getElementById("newGame")

function initGui(){
  title.innerHTML       = "<h1>Blackjack</h1>";
  dealername.innerHTML  = "<h2>Dealer:</h2>";
  playername.innerHTML  = "<h2>Speler:</h2>";
  hitbutton.innerHTML   = '<input type="button" value="Hit" id="hit" onclick="button(\'/hit\');">';
  standbutton.innerHTML = '<input type="button" value="Stand" id="stand" onclick="button(\'/stand\');">';
  resetbutton.innerHTML = '<input type="button" value="New Game" id="newGame" onclick="button(\'/newgame\');">';
}

function renderTable() { 
  let xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      let response = JSON.parse(this.responseText);

      hitbutton.style.visibility = response["buttonState"];
      standbutton.style.visibility = response["buttonState"];
      playerscore.innerHTML = "score:"+response["scores"]["playerscore"];
      dealerscore.innerHTML = "score:"+response["scores"]["dealerscore"];
      message.innerHTML = response["message"];

      renderCards(response["hands"]);
    }
  }
  let route = '/get_gamestate';
  xhttp.open('GET', route);
  xhttp.send();
}

function renderCards(handsarray){
  playerhand.innerHTML = '';
  dealerhand.innerHTML = '';

  // gebruik const hand bij declaratie
  for (hand in handsarray){
    (function(hand) { 
    // gebruik const hacardsnd bij declaratie
      for(cards in handsarray[hand]){
        let src = document.getElementById(hand);
        let img = document.createElement("img");
        img.src = "/renderer/cardimages/" + handsarray[hand][cards]['filename'];
        src.appendChild(img);
      }
    }
    (hand));
  }
}

function button(route){
  var myRequest  = new XMLHttpRequest();
  myRequest.open('GET', route);
  myRequest.send();
  renderTable();
}

