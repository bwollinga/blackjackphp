<?php

class Player {
    function __construct($isDealer = false){
        $this->hand = [];
        $this->points = 0;
        $this->isDealer = $isDealer;
    }
}

class Dealer extends Player {
    function __construct($isDealer = true){
        parent::__construct($isDealer);
        $this->deck = new Deck;
    }

    function shuffleDeck(){
        // for($x = 0;$x < 500 ;$x++){
        //     $taken = array_splice($this->deck->cards,rand(0,51),1);
        //     array_push($this->deck->cards,$taken[0]);
        // }
    }

    function dealCard($player){
        $drawnCard = array_splice($this->deck->cards,0,1);
        array_push($player->hand, $drawnCard[0]);
        $player->points += $drawnCard[0]->point;      
    }

    function evaluateFirstTurn($player){
        if($player->points > 21){
            $this->reduceAce($player);
        }
        if($this->points > 21){
            $this->reduceAce($this);
        }
        if($this->points == 21 && $player->points == 21){
            return "Dealer and player have blackjack! Dealer has won!";
        }
        if($this->points == 21){
            return "Dealer has blackjack! Dealer has won! ";
        }
        else{
            return "";
        }
    }

    function evalHitRequest($player){
        if($player->points <= 21) { 
            $this->dealCard($player);
            while($player->points > 21){
                if(!$this->reduceAce($player)){
                    break;
                }
            }
            if($player->points > 21){
                return false;
            }
            return true;
        }
        return false;
    }

    function reduceAce($player){
        for($i = 0;$i < count($player->hand);$i++){
            if ($player->hand[$i]->value == "Aas" && $player->hand[$i]->point == "11"){
                $player->hand[$i]->value = 1;
                $player->points -= 10;
                return true;
            }
        }
    }

    // mooie oplossing met recursie!
    function runDealerTurn($player){
        // als je 3 asen krijgt gaat het mis, je verliest dan met 23 punten, terwijl je eigenlijk 13 punten zou moeten krijgen?
        if($this->points > 21){
            $this->reduceAce($this);
        }
        if($this->points > 21){
            return "Player has won!";
        }
        if ($this->points > 16 && $this->points > $player->points){
            return "Dealer has won!";
        }
        if($this->points > 16 && $this->points < $player->points){
            return "Player has won!";
        }
        if($this->points < 17 && $this->points > $player->points){
            return "Dealer has won!";
        }
        if($this->points > 16 && $this->points == $player->points){
            return "Game is a draw!";
        }
        if($this->points > 16 && $this->points < $player->points){
            return "Dealer has won!";
        }
        if($this->points < 17){
            $this->dealCard($this);
        }
        return $this->runDealerTurn($player);
    }
 
}

