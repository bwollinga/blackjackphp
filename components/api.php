<?php

class Api {
    public function __construct($engine) {
        $this->engine = $engine;
    }

    function route($uri) {
        switch($uri) {
            case '/':
                require 'pages/home.php';
                break;

            case '/get_gamestate':
                // deze logica zou je nog in een aparte functie van de engine kunnen zetten
                // om de router alleen routing te laten doen
                $handsArray = [
                    "playerhand" => $this->engine->player->hand,
                    "dealerhand" => $this->engine->dealer->hand];
                
                $scoresArray = [
                    "playerscore" => $this->engine->player->points,
                    "dealerscore" => $this->engine->dealer->points];

                $sendArray = [
                    "hands"       => $handsArray,
                    "scores"      => $scoresArray,
                    "message"     => $this->engine->message,
                    "buttonState" => $this->engine->buttonState];

                print (json_encode($sendArray));
                break;

            case '/hit':
                // deze logica zou je nog in een aparte functie van de engine kunnen zetten
                // om de router alleen routing te laten doen
                // gebruik bij voorkeur === in tests zodat datatype ook gelijk moet zijn
                if($this->engine->dealer->evalHitRequest($this->engine->player) == false)
                {
                    $this->engine->message = "Player has lost.";
                    $this->engine->buttonState = "hidden";
                }
                break;

            case '/stand':
                // deze logica zou je nog in een aparte functie van de engine kunnen zetten
                // om de router alleen routing te laten doen
                if($this->engine->message == ''){
                    $resultMessage = $this->engine->dealer->runDealerTurn($this->engine->player);
                    $this->engine->message = $resultMessage;
                    $this->engine->buttonState = "hidden";
                }
                break;

            case '/newgame':
                SESSION_destroy();
                break;

            default:
                exit('No route found');
        }
    }
}
?>
