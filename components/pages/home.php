<!DOCTYPE html>
<html>
<head>
    <!-- voeg altijd een charset meta tag toe -->
    <!-- voeg altijd een title toe -->
</head>
<body>
    <!-- spaties mogen weg voor en na = teken -->
    <div id = "title"></div>
    <div id = "dealername"></div>            
    <div id = "dealerhand"></div>
    <div id = "dealerscore"></div>
    <div id = "playername"></div>
    <div id = "playerhand"></div>
    <div id = "playerscore"></div>
    <div id = "hit"></div>
    <div id = "stand"></div>
    <div id = "message"></div>
    <div id = "newGame"></div>
    
    <!-- deze onload zou je nog in JavaScript kunnen zetten om nog beter HTML en JS te scheiden -->
    <body onload="initGui();renderTable();">
    <script src="renderer\renderer.js"></script>

</body>

</html>
