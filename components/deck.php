<?php

class Deck {
    function __construct(){
        $this->cards = [];

        $suits = ["schoppen","ruiten","klaver","harten"];
        $face = ["Aas", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Boer", "Vrouw", "Koning"];
        $points = [11,2,3,4,5,6,7,8,9,10,10,10,10];

        for($x = 0; $x < count($suits); $x++){
            for($y = 0; $y < count($face); $y++){
                
                // hier zou je een aparte Card class voor kunnen maken
                $card = new stdClass();
                
                $card->value = $face[$y];
                $card->point = $points[$y];
                $card->suit = $suits[$x];
                $card->filename = $suits[$x].$face[$y].".png";

                array_push($this->cards, $card);
            }
        }
    }
}

        
